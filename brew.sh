#!/bin/sh

#
# Brew packages that I use alot.
#
brew install wget
brew install git
brew install git-lfs
brew install gcc
brew install bash-completion
brew install ffmpeg
brew install imagemagick
brew install rbenv
brew install node
brew install freetds
brew install gifsicle
brew tap buo/cask-upgrade
brew install https://raw.githubusercontent.com/kadwanev/bigboybrew/master/Library/Formula/sshpass.rb

# AWS
brew install awscli
brew install awsebcli
brew install docker-credential-helper-ecr

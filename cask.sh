#!/bin/sh

export HOMEBREW_CASK_OPTS="--appdir=/Applications"
# Dev
brew cask install sublime-text
#brew cask install webstorm
#brew cask install rubymine
brew cask install java
#brew cask install sequel-pro
brew cask install kdiff3
brew cask install postman
brew cask install iterm2
brew cask install macdown
brew cask install mono-mdk
brew cask install dotnet-sdk
#Containers and stuff 
#brew cask install docker

# Browser
brew cask install google-chrome
brew cask install firefox

# Utilities
#brew cask install xquartz
#brew cask install airflow
brew cask install spectacle
brew cask install appcleaner
#brew cask install qlcolorcode qlstephen qlmarkdown quicklook-json qlprettypatch quicklook-csv betterzipql qlimagesize webpquicklook suspicious-package quicklookase qlvideo

# Communication
#brew cask install skype
brew cask install slack
brew cask install whatsapp

# Entertaiment
brew cask install vlc

brew cleanup

# Ruby & Rails
rbenv install 2.3.2
rbenv global 2.3.2
gem install nokogiri -- --use-system-libraries
gem install sass
gem install rails

# Node
npm i -g npm
npm install -g typescript
npm install -g angular
npm install -g @angular/cli